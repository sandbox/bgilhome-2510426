<?php

/**
 * @file
 * Contains SearchApiReplaceFilter.
 */

/**
 * Processor for search/replace on text fields.
 */
// @todo Process query?
class SearchApiReplaceFilter extends SearchApiAbstractProcessor {

  public function __construct(SearchApiIndex $index, array $options = array()) {
    parent::__construct($index, $options);
    $this->options += array(
      'search' => '',
      'replace'   => '',
    );
  }

  public function configurationForm() {
    $form = parent::configurationForm();
    $form += array(
      'search' => array(
        '#type' => 'textfield',
        '#title' => t('Search'),
        '#default_value' => $this->options['search'],
      ),
      'replace' => array(
        '#type' => 'textfield',
        '#title' => t('Replace'),
        '#default_value' => $this->options['replace'],
      ),
    );

    return $form;
  }

  public function configurationFormValidate(array $form, array &$values, array &$form_state) {
    parent::configurationFormValidate($form, $values, $form_state);
    if (preg_match($form['search']['#value'], NULL) === FALSE) {
      form_error($form['search'], 'Search text is not a valid regular expression.');
    }
  }

  protected function processFieldValue(&$value) {
    $value = preg_replace($this->options['search'], $this->options['replace'], $value);
  }

}
